

class Hand {
   val dices = mutableListOf<Dice>()

    fun rollDices (){

        if(dices.isEmpty()){
            for(i in 0..5){
                val a = Dice()
                a.rollDice()
                dices.add(a)
            }
        }
        else {
            for (i in 0..5) {
                if (!(dices[i].locked)) {
                    dices.removeAt(i)
                    val b = Dice()
                    b.rollDice()
                    dices.add(b)
                }
            }
        }

        print("This is your hand: ")
        for(i in 0..5){

            print("\t ${dices[i].number}")

        }

        println("")
    }
    fun lockDices(a:List<Int>){

        print("You locked: ")
        for (i in 0 until a.size){
            val c= a[i]
            dices[c].lockDice()
            print("\t${a[i]+1}. dice")
        }
        println("")

    }


}