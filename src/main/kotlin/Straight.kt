class Straight (var big_straight:Boolean = false, var small_straight:Boolean=false):checkValues {
    override fun check(hand: Hand) {

        var c1: Int = 0;
        var c2: Int = 0;
        var c3: Int = 0;
        var c4: Int = 0;
        var c5: Int = 0;
        var c6: Int = 0
        for (i in 0..5) {
            if (hand.dices[i].number == 1)
                c1++
        }
        for (i in 0..5) {
            if (hand.dices[i].number == 2)
                c2++
        }
        for (i in 0..5) {
            if (hand.dices[i].number == 3)
                c3++
        }
        for (i in 0..5) {
            if (hand.dices[i].number == 5)
                c4++
        }
        for (i in 0..5) {
            if (hand.dices[i].number == 5)
                c5++
        }
        for (i in 0..5) {
            if (hand.dices[i].number == 6)
                c6++
        }
        if (c1 == 1 && c2 == 1 && c3 == 1 && c4 == 1 && c5 == 1 && c6 == 1) {
            big_straight = true;
            small_straight = true;
            println("Big Straight and Small Straight in One!")
        } else if (c1 == 1 && c2 == 1 && c3 == 1 && c4 == 1 && c5 == 1) {
            small_straight = true;
            println("Small Straight!")
        }
        else if(c2 == 1 && c3 == 1 && c4 == 1 && c5 == 1 && c6 == 1){
            big_straight = true;
            println("Big Straight!")
        }
        else {
            println("Not Straight!")
        }
    }
}