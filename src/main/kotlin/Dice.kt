

class Dice (var number:Int=0, var locked:Boolean=false) {

    fun rollDice():Int{
        number = (1..6).random()
        return this.number
    }
    fun lockDice(){
        this.locked=true;
    }
}